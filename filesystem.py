def loadInvited():
    result = set()
    import csv

    with open("invited.txt") as names:
        for name in names:
            result.add(str(name).rstrip().lower())

    return result


def loadWhitelist():
    result = set()

    try:
        with open("whitelist.txt", "r") as f:
            for line in f.readlines():
                result.add(int(line))
    except:
        pass

    return result
