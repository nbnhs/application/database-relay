from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import parse_qs
from os import environ
import cgi
import relay
import ssl

class GP(BaseHTTPRequestHandler):
    def _cors_headers(self):
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.send_header("Access-Control-Allow-Headers",
                         "Origin, X-Requested-With, Content-Type, Accept, X-Teach-Name1, X-Teach-Name2, X-Student-Name1, X-Student-Name2, X-Teach-Email, X-NHS-ID, X-NHS-ImageKind")

    def _set_headers(self, content_type):
        self._cors_headers()
        self.send_header('Content-Type', content_type)
        self.end_headers()

    def do_OPTIONS(self):
        self._cors_headers()
        self.end_headers()

    def do_HEAD(self):
        self._cors_headers()
        self.end_headers()

    def do_GET(self):
        # print(self.path)
        data = parse_qs(self.path[2:])
        result = relay.interpret(data)
        if type(result) is str:
            if str(result) == "Failure":
                self.send_response(400)
            self._set_headers("text/html")
            self.wfile.write(bytes(result, "utf-8"))
        else:
            self._set_headers(result[1])
            self.wfile.write(result[0])

    def do_POST(self):
        self._set_headers("text/html")
        # <--- Gets the size of data
        content_length = int(self.headers['Content-Length'])
        # <--- Gets the data itself
        post_data = self.rfile.read(content_length)

        if self.headers['X-Teach-Name1'] is not None:
            self.wfile.write(bytes(relay.send_teacher_invite((self.headers['X-Teach-Name1'], self.headers['X-Teach-Name2']), (
                self.headers['X-Student-Name1'], self.headers['X-Student-Name2']), self.headers['X-Teach-Email']), "utf-8"))
        else:
            self.wfile.write(bytes(relay.add_image(
                ID=self.headers["X-NHS-ID"], sender=self.headers['X-NHS-ImageKind'], imgData=post_data, format=self.headers["Content-Type"].split("/")[1]), "utf-8"))


def run(server_class=HTTPServer, handler_class=GP, port=443):
    server_address = ('0.0.0.0', port)
    httpd = server_class(server_address, handler_class)
    if port == 443:
        httpd.socket = ssl.wrap_socket(
            httpd.socket, keyfile='../privkey.pem', certfile='../fullchain.pem', server_side=True)
    print('Server running at {}:{}...'.format(*server_address))
    httpd.serve_forever()

try:
    run(port=int(environ['PORT']))
except KeyError:
    run()
