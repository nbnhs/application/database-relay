import socket
import sys
import json
import boto3
import requests
from io import BytesIO
from os import environ
from os import path
from google.cloud import datastore
import log
import filesystem

whitelistedIDs = filesystem.loadWhitelist()
invited = filesystem.loadInvited()


def create_applicant(data):
    """ Creates a new object of type Applicant in Google Cloud Datastore. Meant to be used with GET requests.

    :param data: parsed query string in the form `data['key'][0]`. Must contain keys 'id' (number) and 'properties' (JSON).

    :returns: either "Success" or "Failure"
    """

    if len(whitelistedIDs) > 0 and int(data["id"][0]) not in whitelistedIDs:
        log.log(
            "- Whitelist exists and {} isn't on it; failing!".format(str(data["id"][0])))
        return "Failure"

    log.log("Interpreting as request to create applicant")
    client = datastore.Client()

    properties = json.loads(data['properties'][0])
    log.log("- Checking to see if applicant was invited and account not created")

    if "{} {}".format(properties["first_name"].strip().lower(), properties["last_name"].strip().lower()) not in invited:
        log.log("- Applicant wasn't invited; failing")
        return "Failure"

    log.log("- Applicant was invited; proceeding")

    key = None
    try:
        key = client.key("Applicant", int(data["id"][0]))
    except ValueError:
        log.log("Got null for applicant ID!")
        log.log("Name: {} {}".format(
            properties["first_name"], properties["last_name"]))
        return "Failure"

    entityAlreadyExists = client.get(key) is not None
    allFourAreIncomplete = properties["leadership"] == properties[
        "service"] == properties["signature"] == properties["teacher"] == "Incomplete"

    if entityAlreadyExists and allFourAreIncomplete:
        log.log(
            "- Account already exists, and account creation is being attempted; failing")
        return "Failure"

    new_entity = datastore.Entity(key=key)

    for key, val in properties.items():
        log.log("- Adding {}: {}".format(key, val))
        new_entity[key] = val

    try:
        client.put(new_entity)
    except:
        log.log("- Failed")
        return "Failure"

    log.log("- Done")
    return "Success"


def get_applicant_properties(data):
    """ Gets the properties of an object of type Applicant from Google Cloud Datastore. Meant to be used with GET requests.

    :param data: parsed query string in the form `data['key'][0]`. Must contain key 'id' (number).

    :returns: stringified JSON of the Applicant's properties.
    """

    if len(whitelistedIDs) > 0 and int(data["id"][0]) not in whitelistedIDs:
        log.log(
            "- Whitelist exists and {} isn't on it; failing!".format(str(data["id"][0])))
        return "Failure"

    log.log("Interpreting as request to get applicant properties")
    client = datastore.Client()

    key = None
    try:
        key = client.key("Applicant", int(data["id"][0]))
    except ValueError:
        log.log("Got null for applicant ID!")
        return "- Failed"

    entity = client.get(key)
    log.log("- Done")
    return str(json.dumps(dict(entity)))


def authenticate_faculty(data):
    """ Confirms the existence of a Faculty object in Google Cloud Datastore. Meant to be used with GET requests.

    :param data: parsed query string in the form `data['key'][0]`. Must contain key 'id' (number).

    :returns: Either "Failure" or stringified JSON of Faculty's properties.
    """

    log.log("Interpreting as request to authenticate faculty member")
    client = datastore.Client()
    key = client.key("Faculty", int(data["id"][0]))

    entity = client.get(key)

    try:
        log.log("- Done")
        return str(json.dumps(dict(entity)))
    except:
        log.log("- Failed")
        return "Failure"


def authenticate_admin(data):
    """ Confirms the existence of an Admin object in Google Cloud Datastore. Meant to be used with GET requests.

    :param data: parsed query string in the form `data['key'][0]`. Must contain key 'adminID' (number).

    :returns: Either "Failure" or stringified JSON of Admin's properties.
    """

    log.log("Interpreting as request to authenticate admin")
    client = datastore.Client()
    key = client.key("Admin", int(data["adminID"][0]))

    entity = client.get(key)

    try:
        log.log("- Done")
        return str(json.dumps(dict(entity)))
    except:
        log.log("- Failed")
        return "Failure"


def put_path_in_applicant(url, path_type, ID, check=True):
    """ Properly places a URL into the given Applicant.

    :param url: URL to place inside Applicant.
    :param path_type: Either "teacher", "service", or "signature"; used to determine where URL should go in Applicant.
    :param ID: Applicant ID; used to retrieve Applicant from GCloud Datastore.
    """

    if check:
        if len(whitelistedIDs) > 0 and ID not in whitelistedIDs:
            log.log(
                "- Whitelist exists and {} isn't on it; failing!".format(str(ID)))
            return "Failure"

    client = datastore.Client()
    key = client.key("Applicant", ID)

    entity = client.get(key)
    entity[path_type] = url

    client.put(entity)
    log.log("- Put path {} in {}".format(url, path_type))


def add_image(data=None, ID=None, sender=None, imgData=None, format=None):
    """ Uploads an image to the NHS Amazon S3 bucket, using the given Applicant ID and request sender to properly place the image under the directory structure `Applicant_ID/sender.format`.

    :param data: (Optional) parsed query string in the form `data['key'][0]`. Must contain keys "sender", "data", "id", and "format". Only specify this parameter if using GET requests, since all other parameters will be ignored if this one exists.
    :param ID: (Optional) ID of the Applicant for who the image corresponds to. Do not specify the data parameter if using this one.
    :param sender: (Optional) any of ["dean", "guide", "teach"]. Do not specify the data parameter if using this one.
    :param imgData: image data. Do not specify the data parameter if using this one.
    :param format: image format (e.g. png, jpeg). Do not specify the data parameter if using this one.

    :returns: "Success" if succeded. Will raise an exception otherwise.
    """

    s3 = boto3.resource('s3')
    bucket = s3.Bucket('nhs-applications')

    # This clause (if data is not None) is legacy code.
    # It addresses the deprecated and unused feature of this server to accept
    # image files and JSON input as a GET request.
    # Refer to the (else) clause for the actual working code.
    if data is not None:
        sender_manual = data["sender"][0]
        if sender_manual not in ["dean", "guide", "teach"]:
            return "Invalid sender"

        log.log("Request to add image (type: {}, for: {})".format(
            sender_manual, data["id"][0]))
        imgData_manual = bytes(data["data"][0])

        if sender_manual != "teach":
            if len(whitelistedIDs) > 0 and int(data["id"][0]) not in whitelistedIDs:
                log.log("- Whitelist exists and the requester isn't on it! Failing.")
                return "Failure"

        url = "{id}/{sender}.{format}".format(
            id=data["id"][0], sender=sender_manual, format=data["format"][0])
        bucket.put_object(Key=url, Body=imgData_manual)

        put_path_in_applicant(url, sender_manual,
                              int(data["id"][0]), check=False)
        log.log("- Done")
        return "Success"
    else:
        log.log("Request to add image (type: {}, for: {})".format(sender, ID))

        if sender != "teacher":
            if len(whitelistedIDs) > 0 and int(ID) not in whitelistedIDs:
                log.log("- Whitelist exists and the requester isn't on it! Failing.")
                return "Failure"

        url = "{id}/{sender}.{format}".format(id=ID,
                                              sender=sender, format=format)
        bucket.put_object(Key=url, Body=imgData)

        put_path_in_applicant(url, sender, int(ID), check=False)
        log.log("- Done")
        return "Success"


def send_email(dest, subject, html_message):
    """ Sends an email with the Mailgun API.

    :param dest: Destination email address. Can be formatted as `Hello There <hello@example.com>`.
    :param subject: Email subject.
    :param html_message: Message body (as HTML).

    :returns: Response from Mailgun API server.
    """
    mailgun_key = environ['MAILGUN_API_KEY']
    params = {"from": "North Broward National Honor Society <mailgun@mail.nbnhs.org>",
              "to": dest, "subject": subject, "html": html_message}

    r = requests.post("https://api.mailgun.net/v3/mail.nbnhs.org/messages",
                      data=params, auth=('api', mailgun_key))
    log.log("- POSTed to Mailgun. Response: {}".format(r.text))
    return json.loads(r.text)['message']


def send_final_notice_email(data):
    log.log("Interpreting as request to send final notices")

    if (authenticate_admin(data) == "Failure"):
        log.log("- Failed: cannot auth admin")
        return "Unable to authenticate admin"

    res = json.loads(get_incomplete(data))

    log.log("- Got incomplete apps.")

    for app in res:
        incompleteItemNames = []
        if app["leadership"] == "Incomplete":
            incompleteItemNames.append("<li>Leadership</li>")
        if app["service"] == "Incomplete":
            incompleteItemNames.append("<li>Service</li>")
        if app["teacher"] == "Incomplete":
            incompleteItemNames.append("<li>Teacher recommendation</li>")
        if app["signature"] == "Incomplete":
            incompleteItemNames.append("<li>Signature</li>")

        message = """
<h1 id="hello">Hello {},</h1>

<p>One or more of the materials required for the National Honor Society to make a decision on your application have not been received.</p>

<p>The following must be submitted prior to 8 AM on Feb. 3rd for your application to be evaluated:</p>

<ul>
{}
</ul>

<p>This is the last notice we'll send regarding incomplete items. It is your responsibility to make sure all items are marked "Pending" before the aforementioned deadline.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>
""".format(app["first_name"], "\n".join(incompleteItemNames))

        send_email(app["email"], "Your NHS Application Is Incomplete", message)

    return "Success"


def send_signup_confirmation(data):
    log.log("Interpreting as request to send applicant a confirmation email")
    client = datastore.Client()

    key = None
    try:
        key = client.key("Applicant", int(data["id"][0]))
    except ValueError:
        log.log("Got null for applicant ID!")
        return "- Failed"

    entity = dict(client.get(key))

    message = """
<h1 id="hello">Hello {student_first_name},</h1>

<p>You have successfully registered an applicant account with the National Honor Society.</p>

<p>You may fill out your application at https://app.nbnhs.org by logging in with your ID card number.</p>

<p>For your reference, the ID card number that you provided to us was {id}.</p>

<p>We strongly recommend completing the application on a computer running Chrome, Firefox, or Safari.</p>

<p>Thank you for your interest in the National Honor Society! We can't wait to get to know you.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>
""".format(student_first_name=entity["first_name"], id=data["id"][0])

    return send_email(entity["email"], "Registration with the National Honor Society", message)


def generate_teacher(name_tuple):
    """ Generates a new Faculty object in GCloud Datastore with a randomly generated ID if one does not already exist.

    :param name_tuple: Tuple in the form (first_name, last_name).

    :returns: ID of the new Faculty object. Returns ID of already existing object if one is found.
    """

    client = datastore.Client()

    # Check if teacher already exists
    query = client.query(kind="Faculty")
    query.add_filter('name', '=', "{} {}".format(*name_tuple))
    results = list(query.fetch())
    if results:
        log.log(
            "- Teacher with name {} {} already exists, re-using entry in database".format(*name_tuple))
        return results[0].id

    incomplete_key = client.key("Faculty")
    key = client.allocate_ids(incomplete_key, 1)
    if type(key) is list:
        key = key[0]

    new_entity = datastore.Entity(key=key)
    new_entity['name'] = "{} {}".format(*name_tuple)

    client.put(new_entity)
    return new_entity.id


def send_teacher_invite(name_tuple, student_name_tuple, email):
    """ Sends an invite link to the teacher in question.

    :param name_tuple: Tuple in the form (first_name, last_name) of the teacher.
    :param student_name_tuple: Tuple in the form (first_name, last_name) of the student.
    :param email: Teacher's email. Must end in "@nbps.org".
    """

    log.log("Sending teacher invite: from {} {}, to {} {} at {}".format(
        *student_name_tuple, *name_tuple, email))
    if email.split("@")[1] != "nbps.org":
        log.log("- Failure: {} does not end in nbps.org".format(email))
        return "Email does not end in nbps.org."

    url = "http://faculty.nbnhs.org?token={}".format(
        generate_teacher(name_tuple))
    message = """
<h1 id="hello">Hello {teacher_full_name},</h1>

<p>Your student {student_full_name} has invited you to fill out their Teacher Recommendation Form for the North Broward National Honor Society.</p>

<p>Please visit the website at {url} once you've filled in the paper copy of the form in order to upload it.</p>

<p>Thank you in advance for helping the NHS make a decision about {student_first_name}.</p>

<p>Sincerely yours,</p>

<p>Milo Gilad<p>

<p>NHS President</p>

<p>(<strong>Note</strong>: this email does not accept replies. Email <a href="mailto:milo.gilad@mynbps.org">milo.gilad@mynbps.org</a> with any questions.)</p>
""".format(teacher_full_name="{} {}".format(*name_tuple), student_full_name="{} {}".format(*student_name_tuple), url=url, student_first_name=student_name_tuple[0])

    result = send_email(
        email, "National Honor Society Teacher Recommendation Request", message)
    log.log("- Done")
    return result


def get_completed(data):
    """Gets a list of complete Applicants.

    :param data: parsed query string in the form `data['key'][0]`. Must contain key "adminID".
    :returns: 2-dimensional array of every Applicant with no fields marked as "Pending"
    """
    log.log("Interpreting as request to get completed applications")

    if (authenticate_admin(data) == "Failure"):
        log.log("- Failed: cannot auth admin")
        return "Unable to authenticate admin"

    client = datastore.Client()
    query = client.query(kind="Applicant")
    # query.add_filter('id', '>', 0)
    results = list(query.fetch())

    res = []
    for app in results:
        if app['leadership'] != "Incomplete" and app['service'] != "Incomplete" and app['teacher'] != "Incomplete" and app['signature'] != "Incomplete":
            res.append(app)

    return json.dumps(res)


def get_incomplete(data):
    """Gets a list of complete Applicants.

    :param data: parsed query string in the form `data['key'][0]`. Must contain key "adminID".
    :returns: 2-dimensional array of every Applicant with no fields marked as "Pending"
    """
    log.log("Interpreting as request to get incomplete applications")

    if (authenticate_admin(data) == "Failure"):
        log.log("- Failed: cannot auth admin")
        return "Unable to authenticate admin"

    client = datastore.Client()
    query = client.query(kind="Applicant")
    # query.add_filter('id', '>', 0)
    results = list(query.fetch())

    res = []
    for app in results:
        if app['leadership'] == "Incomplete" or app['service'] == "Incomplete" or app['teacher'] == "Incomplete" or app['signature'] == "Incomplete":
            res.append(app)

    return json.dumps(res)


def get_all_with_props(data):
    """Gets a list of Applicants that satisfy the given conditions..

    :param data: parsed query string in the form `data['key'][0]`. Must contain key "adminID".
    :returns: 2-dimensional array of every Applicant with no fields marked as "Pending"
    """
    log.log("Interpreting as request to get incomplete applications")

    if (authenticate_admin(data) == "Failure"):
        log.log("- Failed: cannot auth admin")
        return "Unable to authenticate admin"

    client = datastore.Client()
    query = client.query(kind="Applicant")
    # query.add_filter('id', '>', 0)
    results = list(query.fetch())

    res = []
    properties = dict(json.loads(data['properties'][0]))

    for app in results:
        for key in properties.keys():
            if properties[key] != app[key]:
                break
            res.append(app)

    return json.dumps(res)


def get_final_csv_report(data):
    if (authenticate_admin(data) == "Failure"):
        log.log("- Failed: cannot auth admin")
        return "Unable to authenticate admin"

    complete = json.loads(get_completed(data))
    incomplete = json.loads(get_incomplete(data))
    csv = "First Name,Last Name,Status\n"

    def tallyPoints(score):
        arr = str(score).split(",")
        arr.pop()
        return sum(int(i) for i in arr)
    
    def caseName(string):
        return ' '.join([i.capitalize() for i in string.split(" ")])

    for app in complete:
        csv += "{},{},".format(caseName(app["first_name"].strip()),
                               caseName(app["last_name"].strip()))
        if tallyPoints(app["points"]) >= 7:
            csv += "Accepted\n"
        else:
            csv += "Rejected\n"

    for app in incomplete:
        csv += "{},{},Incomplete\n".format(caseName(app["first_name"].strip()),
                                           caseName(app["last_name"].strip()))

    # Now, grab people who were invited but never made an account
    registered_names = ["{} {}".format(i["first_name"].lower(), i["last_name"].lower()) for i in complete] + [
        "{} {}".format(i["first_name"].lower(), i["last_name"].lower()) for i in incomplete]

    not_registered = set(invited).difference(registered_names)

    for name in not_registered:
        csv += "{},{},Never Started\n".format(caseName(str(name).split(" ")[0]),
                                              caseName(' '.join(str(name).split(" ")[1:])))

    return csv


def get_image(data):
    """Gets an image from Amazon S3.

    :param data: parsed query string in the form `data['key'][0]`. Must contain keys "adminID" and "path".
    :returns: binary data constituting an image
    """

    if (authenticate_admin(data) == "Failure"):
        return "Unable to authenticate admin"

    s3 = boto3.resource('s3')

    try:
        bytes_io = BytesIO()
        img_data = s3.Object('nhs-applications',
                             data['path'][0]).download_fileobj(bytes_io)
        return (bytes_io.getvalue(), "image/{}".format(data['path'][0].split(".")[1]))
    except Exception as e:
        return "{}".format(e)


def set_applicant_points(data):
    """Sets the number of points an Applicant has accrued on their application.

    :param data: parsed query string in the form `data['key'][0]`. Must contain keys "adminID," "applicantID," and "points."
    """
    log.log("Interpreting as request to set applicant points")
    if (authenticate_admin(data) == "Failure"):
        return "Unable to authenticate admin"

    client = datastore.Client()
    key = client.key("Applicant", int(data["applicantID"][0]))
    entity = client.get(key)
    entity['points'] = data["points"][0]

    try:
        client.put(entity)
    except:
        log.log("- Failed")
        return "Failure"

    log.log("- Done")
    return "Success"


def interpret(data):
    """Interprets a GET request query string processed with `parse_qs`.

    :param data: parsed query string in the form `data['key'][0]`
    """

    log.log("GET request received: {}".format(str(data)))

    if data['operation'][0] == "set":
        return create_applicant(data)
    elif data['operation'][0] == "get":
        return get_applicant_properties(data)
    elif data['operation'][0] == "sendconfirmation":
        return send_signup_confirmation(data)
    elif data['operation'][0] == "authfaculty":
        return authenticate_faculty(data)
    elif data['operation'][0] == "authadmin":
        return authenticate_admin(data)
    elif data['operation'][0] == "addImg":
        return add_image(data)
    elif data['operation'][0] == "getCompleted":
        return get_completed(data)
    elif data['operation'][0] == "getIncomplete":
        return get_incomplete(data)
    elif data['operation'][0] == "getFromAWS":
        return get_image(data)
    elif data['operation'][0] == "setPoints":
        return set_applicant_points(data)
    elif data['operation'][0] == "getAllWith":
        return get_all_with_props(data)
    elif data['operation'][0] == "finalNotice":
        return send_final_notice_email(data)
    elif data['operation'][0] == "finalReport":
        return get_final_csv_report(data)
    else:
        return "{} is not a valid operation".format(data['operation'][0])
