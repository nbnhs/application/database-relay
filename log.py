import os


def log(s):
    print(s)
    with open('relay.log', 'a') as f:
        f.write(s + "\n")
